﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Actions/Detonate")]
public class DetonateAction : Action
{
    public override void Act(StateController controller)
    {
        Detonate(controller);
    }

    private void Detonate(StateController controller)
    {
        controller.chargerDetonating.Detonate();

    }
}
