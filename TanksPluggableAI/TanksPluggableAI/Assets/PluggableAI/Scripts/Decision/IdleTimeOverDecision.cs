﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/IdleTimeOver")]
public class IdleTimeOverDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool targetVisible = Timed(controller);
        return targetVisible;
    }

    private bool Timed(StateController controller)
    {
        return controller.CheckIfCountDownElapsed(controller.enemyStats.idleDuration);
    }
}


