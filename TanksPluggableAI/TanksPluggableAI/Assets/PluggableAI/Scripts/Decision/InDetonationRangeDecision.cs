﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "PluggableAI/Decisions/InDetonationRange")]
public class InDetonationRangeDecision : Decision
{

    public override bool Decide(StateController controller)
    {
        bool targetInRange = InDetonationRange(controller);
        return targetInRange;
    }

    private bool InDetonationRange(StateController controller)
    {
        return Vector3.Distance(controller.transform.position, controller.chaseTarget.position) <= controller.enemyStats.detonationRange;
    }

}


